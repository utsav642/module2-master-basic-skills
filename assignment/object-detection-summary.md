# Object Detection  

We all are aware about the ongoing research in "self driving cars".  
It means that car needs to detect the obstacle in it's way by itself  
and drive accordingly...  

![self-driving-car](https://cdn.vox-cdn.com/thumbor/yGa_DWTQkCbOM4SfzLBAcJLgLBI=/0x0:8086x5391/1320x0/filters:focal(0x0:8086x5391):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/19711037/GettyImages_1198672481.jpg)  
credits : "It’s 2020. Where are our self-driving cars?" by kelsey piper  
source : [link to above article](https://www.vox.com/future-perfect/2020/2/14/21063487/self-driving-cars-autonomous-vehicles-waymo-cruise-uber)  

As far as **2020's** concerned, we see various airports, railway stations  
are trying to detect wheather people are maintaining "social distancing",  
"wearing masks", etc...  

So the point is how will all this happen?  
this is exactly where **_Object Detection_** comes in the picture.  
 
Object detection as the name suggests, basically recognizing different  
objects in the input image...  

### There are three stages in Object Detection,
 *Classification
 *Localization
 *Object Detection
 
 
In a very simple manner,  
 
![Classification - Localization - Object Detection](https://miro.medium.com/max/700/1*spGG34X1S9MlW512UQ86jA.png)  
credits : "Understanding Object Detection and R-CNN" by Aakash Yelisetty   
source : [link to the above "medium" article](https://towardsdatascience.com/understanding-object-detection-and-r-cnn-e39c16f37600)  

classification is just that I am labelling a picture as a "dog" let us suppose  
whereas, in localization, I mark around the dog in the picture...  

and when I differentiate one object from another in the picture containing  
multiple objects, it is nothing but object detection.   

Here using different techniques we identify the features, patterns in the image,  
for example by using "sliding window algorithm" this can be done.

![sliding-window](https://miro.medium.com/max/700/1*zbBVqsIM9eYiQpG5LZ5Opw.gif)  
credits : "Understanding Object Detection and R-CNN" by Aakash Yelisetty   
source : [link to the above "medium" article](https://towardsdatascience.com/understanding-object-detection-and-r-cnn-e39c16f37600)  
 
we have concepts like selective search, image pyramid in this.  
 
##### **_R-CNN performs vital role in this!_**
